package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {

    final Label screen = new Label();
    final Button b0 = new Button("0");
    final Button b1 = new Button("1");
    final Button b2 = new Button("2");
    final Button b3 = new Button("3");
    final Button b4 = new Button("4");
    final Button b5 = new Button("5");
    final Button b6 = new Button("6");
    final Button b7 = new Button("7");
    final Button b8 = new Button("8");
    final Button b9 = new Button("9");
    final Button bPoint = new Button(".");
    final Button bPlus = new Button("+");
    final Button bMinus = new Button("-");
    final Button bMul = new Button("*");
    final Button bDiv = new Button("/");
    final Button bEqual = new Button("=");
    final Button bClear = new Button("Clear");

    @Override
    public void start(Stage primaryStage) throws Exception{

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(25, 25, 25, 25));

        screen.setPrefSize(195,60);
        screen.setStyle("-fx-background-color: rgba(255, 255, 255, 1);");
        b0.setPrefSize(75,35);

        b1.setPrefSize(35,35);
        b2.setPrefSize(35,35);
        b3.setPrefSize(35,35);
        b4.setPrefSize(35,35);
        b5.setPrefSize(35,35);
        b6.setPrefSize(35,35);
        b7.setPrefSize(35,35);
        b8.setPrefSize(35,35);
        b9.setPrefSize(35,35);
        bPoint.setPrefSize(35,35);
        bPlus.setPrefSize(35,35);
        bMinus.setPrefSize(35,35);
        bMul.setPrefSize(35,35);
        bDiv.setPrefSize(35,35);
        bEqual.setPrefSize(35,75);
        bClear.setPrefSize(75,35);

        HBox hb123 = new HBox(5);
        hb123.getChildren().addAll(b1,b2,b3);
        HBox hb456 = new HBox(5);
        hb456.getChildren().addAll(b4,b5,b6);
        HBox hb789 = new HBox(5);
        hb789.getChildren().addAll(b7,b8,b9);
        HBox hb0 = new HBox(5);
        hb0.getChildren().addAll(b0,bPoint);
        VBox vbNum = new VBox(5);
        vbNum.getChildren().addAll(hb123,hb456,hb789,hb0);

        HBox hbPlusMinus = new HBox(5);
        hbPlusMinus.getChildren().addAll(bPlus,bMinus);
        VBox vbMulDiv = new VBox(5);
        vbMulDiv.getChildren().addAll(bMul,bDiv);
        HBox hbOpEq = new HBox(5);
        hbOpEq.getChildren().addAll(vbMulDiv,bEqual);
        VBox vbOperat = new VBox(5);
        vbOperat.getChildren().addAll(hbPlusMinus,hbOpEq,bClear);

        HBox hbButtons = new HBox(5);
        hbButtons.getChildren().addAll(vbNum,vbOperat);

        VBox vbCalc = new VBox(10);
        vbCalc.getChildren().addAll(screen,hbButtons);
        grid.add(vbCalc,1,1);

        // Actions
        takeAction();

        primaryStage.setScene(new Scene(grid, 300, 275));
        primaryStage.setTitle("Calculator");
        primaryStage.show();
    }

    int res = 0;
    ArrayList<Integer> num = new ArrayList<Integer>();
    int numberPointer = 0;
    int digit = 0; // can be from 0 to 9
    ArrayList<Integer> operations = new ArrayList<Integer>(); // can be from 1 to 4 (see below)
    int operationsPointer = -1;
    /* flag turns 0 when we hit a numeric button or Clear
                      1 when we hit +
                      2 when we hit -
                      3 when we hit *
                      4 when we hit /
                      5 when we hit .
                      6 when we hit =
    */
    int flag = -1;

    public void takeAction(){
        num.add(0);
        b0.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 0;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(0);
                }else{
                    res = num.get(numberPointer)*10+0;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 1;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(1);
                }else{
                    res = num.get(numberPointer)*10+1;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 2;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(2);
                }else{
                    res = num.get(numberPointer)*10+2;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 3;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(3);
                }else{
                    res = num.get(numberPointer)*10+3;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 4;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(4);
                }else{
                    res = num.get(numberPointer)*10+4;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 5;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(5);
                }else{
                    res = num.get(numberPointer)*10+5;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b6.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 6;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(6);
                }else{
                    res = num.get(numberPointer)*10+6;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b7.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 7;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(7);
                }else{
                    res = num.get(numberPointer)*10+7;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b8.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 8;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(8);
                }else{
                    res = num.get(numberPointer)*10+8;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        b9.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                digit = 9;
                if(flag == 1 || flag == 2 || flag == 3 || flag == 4){
                    numberPointer ++;
                    num.add(9);
                }else{
                    res = num.get(numberPointer)*10+9;
                    num.remove(numberPointer);
                    num.add(res);
                }
                screen.setText(screen.getText()+digit);
                flag = 0;
            }
        });
        bPlus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(flag == 0){
                    screen.setText(screen.getText()+" + ");
                    operations.add(1);
                    operationsPointer ++;
                    flag = 1;
                }
            }
        });
        bMinus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(flag == 0){
                    screen.setText(screen.getText()+" - ");
                    operations.add(2);
                    operationsPointer ++;
                    flag = 2;
                }
            }
        });
        bMul.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(flag == 0) {
                    screen.setText(screen.getText() + " * ");
                    operations.add(3);
                    operationsPointer++;
                    flag = 3;
                }
            }
        });
        bDiv.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(flag == 0) {
                    screen.setText(screen.getText() + " / ");
                    operations.add(4);
                    operationsPointer++;
                    flag = 4;
                }
            }
        });
        bPoint.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

            }
        });
        bEqual.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                res = num.get(0);
                for(int i=1;i<=numberPointer;i++){
                    if(operations.get(i-1) == 1){
                        res = res + num.get(i);
                    }else if(operations.get(i-1) == 2){
                        res = res - num.get(i);
                    }else if(operations.get(i-1) == 3){
                        res = res * num.get(i);
                    }else{
                        res = res / num.get(i);
                    }
                }
                screen.setText(""+res);
                clearAll();
                num.add(res);
                numberPointer = 0;
            }
        });
        bClear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                clearAll();
                screen.setText("");
                num.add(0);
                flag = -1;
            }
        });
    }

    public void clearAll(){
        num.clear();
        numberPointer = 0;
        operations.clear();
        operationsPointer = -1;
        digit = 0;
        flag = 0;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
